import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';

class NewsLetter extends Component {
	render() {
		return (
			<Fade right>
			    <div className="news_letter_section">
			    	<div className="background_img">
			    		<div className="container">
			    			<div className="row">
			    				<div className="col-md-2">
			    					<img src="/img/strategos-newsletter-icon.png" alt="strategos-newsletter-icon" />
			    				</div>
								<div className="col-md-10">
		    					<h1>
		    						Newsletter Sign Up
		    					</h1>
		    					<p>
		    						Subscribe to Our Latest Insights.
		    					</p>
		    					<form className="form-inline">
								  	<div className="form-group mx-sm-3 mb-2 input">
									    <label htmlFor="inputPassword2" className="sr-only">First Name</label>
									    <input type="text" className="form-control" id="" placeholder="First Name" />
								  	</div>
								  	<div className="form-group mx-sm-3 mb-2 input">
									    <label htmlFor="inputPassword2" className="sr-only">Last Name</label>
									    <input type="text" className="form-control" id="" placeholder="Last Name" />
								  	</div>
								  	<div className="form-group mx-sm-3 mb-2 input">
									    <label htmlFor="inputPassword2" className="sr-only">Email Address</label>
									    <input type="text" className="form-control" id="" placeholder="Email Address" />
								  	</div>
								  	<button type="submit" className="btn btn-primary mb-2">Submit</button>
								</form>
		    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</Fade>
		);
	}
}

export default NewsLetter;
