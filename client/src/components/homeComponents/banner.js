import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import Reveal from 'react-reveal/Reveal';
import {
	Link
} from 'react-router-dom';

class Banner extends Component {
	render() {
		return (
			<Slide left>
			    <div className="home_banner">
			    	<div className="home_banner_overlay"></div>
				    	<div className="banner_caption">
				    		<Reveal effect="fadeInUp" delay={2000}>
					    		<h1>
					    			We are extremely respectful <br/>
					    			of what we don't know.
					    		</h1>
					    		<p>
					    			Our practice is built around the specific areas that we possess <br/>
					    			deep subject matter expertise as a result of our experience. We <br />
					    			will not short-change our clients by advising on areas important <br />
					    			to them that we only possess theoretical knowledge.
					    		</p>
					    		<em>
					    			<b>
					    				We know better
					    			</b>
					    		</em>
					    	</Reveal>
				    	</div>

				    	<div className="banner_img">
				    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
				    		<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
				    	</div>
				    </div>
		    </Slide>
		);
	}
}

export default Banner;
