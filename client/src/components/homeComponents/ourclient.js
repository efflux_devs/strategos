import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';

class OurClient extends Component {
	render() {
		return (
			<Fade right>
			    <div className="ourclient_section module">
			    	<div className="background_img">
			    		<div className="container">
			    			<img src="/img/strategos-cypher-logo.png" className="client_logo" alt=" Strategos Logo " />
			    			<img src="/img/strategos-gwx-logo.png" className="client_logo" alt=" Strategos Logo " />
			    			<img src="/img/strategos-lori-logo.png" className="client_logo" alt=" Strategos Logo " />
			    			<img src="/img/strategos-max-logo.png" className="last_img" alt=" Strategos Logo " />
			    		</div>
			    	</div>
			    </div>
			</Fade>
		);
	}
}

export default OurClient;
