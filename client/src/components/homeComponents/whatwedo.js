import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {Helmet} from "react-helmet";
import {
	Link
} from 'react-router-dom';

const TITLE = 'Home - Strategos'

class WhatWeDo extends Component {
	render() {
		return (
			<Slide left>
			    <div className="whatwedo_section">
			    	<Helmet>
		                <title>{ TITLE }</title>
		            </Helmet>
			    	<h1>
			    		What We Do
			    	</h1>
			    	<div className="strategos-home-what-we-do-background-img">
			    		<div className="container">
			    			<div className="row">
			    				<div className="col-md-6">
			    					<div className="strategos-home-what-we-do-chart"></div>
			    				</div>

			    				<div className="col-md-6">
			    					<ul>
			    						<li>
			    							<Link to="/what-we-do/strategy-and-execution" className="strategos-home-what-we-do-anchor">
			    								<img src="/img/strategos-strategy-icon.png" alt="Strategos Strategy Icon" />
			    								Strategy and Execution
			    							</Link>
		    							</li>
			    						<li>
			    							<Link to="/what-we-do/recruitment" className="strategos-home-what-we-do-anchor">
				    							<img src="/img/strategos-recruitment-icon.png" alt="Strategos Recruitment Icon" />
				    							Recruitment
			    							</Link>
		    							</li>
			    						<li>
			    							<Link to="/what-we-do/investor" className="strategos-home-what-we-do-anchor">
				    							<img src="/img/strategos-investor-icon.png" alt="Strategos Investor Icon" />
				    							Stakeholder Management
			    							</Link>
		    							</li>
			    					</ul>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    	<div className="button">
						<Link to="/what-we-do">
							Read More
						</Link>
					</div>
			    </div>
			</Slide>
		);
	}
}

export default WhatWeDo;
