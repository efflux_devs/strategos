import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';

class OurProcess extends Component {
	render() {
		return (
			<Slide right>
		    	<div className="home_ourprocess_section">
		    		<div className="background_color">
			    		<div className="strategos-img bounce-1">
			    			<h1>
				    			Our Process
				    		</h1>

					    	<p>
					    		<b>We Engage</b><br /><br />
					    		We facilitate purposeful conversations to help clearly<br /> identify our clients 
					    		business pain points and how it <br />prevents their optimal performance.
					    	</p>

				    		<div className="button">
	    						<Link to="/contact">
	    							let's talk
	    						</Link>
	    					</div>
			    		</div>
			   		</div>
			   	</div>
		    </Slide>
		);
	}
}

export default OurProcess;
