import React, { Component } from 'react';
import {
	Link
} from 'react-router-dom';
import Slide from 'react-reveal/Slide';

class WhoWeAre extends Component {
	render() {
		return (
		    <div className="home_whoweare_section">
		    	<div className="background_color">
		    		<div className="container">
		    			<div className="row">
		    				<h1>
					    		Who We Are
					    	</h1>
							<Slide left>
			    				<div className="col-md-6">
			    					<div className="strategos_hockey_stick"></div>
			    				</div>
			    			</Slide>
			    			<Slide right>
			    				<div className="col-md-6">
			    					<p>
			    						We are not consultants. <b>We are managers with significant experience</b> and understanding 
			    						of what it takes to operate in volatile and uncertain markets. <br /> <br />

										Our C-level experience in different organizations and sectors has enabled us to build a practical and 
										outcome-based approach to managing businesses that we bring to bear upon our clients’ pain-points. <br /> <br />

										We work with fast-growing businesses to ensure short-term profitability as well as long-term growth.
			    					</p>
			    					<em>
			    						<Link to="/our-company">
			    							Read More
			    						</Link>
			    					</em>
			    				</div>
		    				</Slide>

		    				<div className="button">
		    					<Link to="/contact">
		    						talk to us
		    					</Link>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		);
	}
}

export default WhoWeAre;
