import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';
import {
	Link
} from 'react-router-dom';

class Footer extends Component {
	render() {
		return (
			<Fade bottom>
			    <div className="footer_section">
			    	<div className="background_img">
			    		<div className="container">
			    			<div className="row">
			    				<div className="col-md-3">
			    					<img src="/img/strategos-logo.png" alt="The Company Logo"/>
			    				</div>

			    				<div className="col-md-3">
			    					<h1>Service Offerings</h1>
			    					<ul>
			    						<li>
			    							<Link to="/what-we-do/strategy-and-execution">Strategy and Execution</Link>
			    						</li>
			    						<li>
			    							<Link to="/what-we-do/recruitment">Recruitment</Link>
			    						</li>
			    						<li>
			    							<Link to="/what-we-do/investor">Stakeholder Management</Link>
			    						</li>
			    					</ul>
			    				</div>

			    				<div className="col-md-3">
			    					<h1>Navigations</h1>
			    					<ul>
			    						<li>
			    							<Link to="/">Home</Link>
			    						</li>
			    						<li>
			    							<Link to="/our-company">Our Company</Link>
			    						</li>
			    						<li>
			    							<Link to="/what-we-do">What We Do</Link>
			    						</li>
			    						<li>
			    							<Link to="/blog">Blog</Link>
			    						</li>
			    						<li>
			    							<Link to="/contact">Contact Us</Link>
			    						</li>
			    					</ul>
			    				</div>

			    				<div className="col-md-3">
			    					<h1>Follow Us</h1>
			    					<ul className="strategoscompany_media">
			    						<li>
			    							<a href=" https://www.instagram.com/thestrategoscompany_ng/" target="_blank" rel="noopener noreferrer">
			    								<i className="fa fa-instagram"></i>
			    							</a>
			    						</li>
			    						<li>
			    							<a href="https://www.facebook.com/strategoscompanyNG/" target="_blank" rel="noopener noreferrer">
			    								<i className="fa fa-facebook"></i>
			    							</a>
			    						</li>
			    						<li>
			    							<a href="https://www.linkedin.com/company/the-strategos-company-ng" target="_blank" rel="noopener noreferrer">
			    								<i className="fa fa-linkedin"></i>
			    							</a>
			    						</li>
			    						<li>
			    							<a href="https://twitter.com/thestrategos_ng" target="_blank" rel="noopener noreferrer">
			    								<i className="fa fa-twitter"></i>
			    							</a>
			    						</li>
			    					</ul>
			    				</div>
			    			</div>
			    			<p className="strategos_p">
			    				&#169;2019 Strategos | Privacy Policy
			    				<b>
			    					<a target="_blank" href="http://onthegotechnologies.com/" rel="noopener noreferrer">
			    						Designed by OnTheGo Technologies
			    					</a>
			    				</b>
			    			</p>
			    		</div>
			    	</div>
			    </div>
		    </Fade>
		);
	}
}

export default Footer;
