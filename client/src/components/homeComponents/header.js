import React, { Component } from 'react';
import Fade from 'react-reveal/Fade'
import {
	Link,
	NavLink
} from 'react-router-dom';


class Header extends Component {
	render() {
		return (
			<Fade up>
			  	<div className="App red-square">
			    	<nav className="navbar navbar-default">
					  <div className="container">
					    <div className="navbar-header">
					      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					        <span className="sr-only">Toggle navigation</span>
					        <span className="icon-bar"></span>
					        <span className="icon-bar"></span>
					        <span className="icon-bar"></span>
					      </button>
					      <a className="navbar-brand" href="/">
					      	<img src="/img/strategos-logo.png" className="stratelogo" alt=" Strategos Logo " />
					      </a>
					    </div>

					    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul className="nav navbar-nav navbar-right">
					        <li className="cl-effect-4">
					        	<NavLink to="/" exact activeClassName="strategos-active">
					        		Home
					        	</NavLink>
				        	</li>

				        	<li className="cl-effect-4">
				        		<NavLink to="/our-company" activeClassName="strategos-active">
				        			Our Company
			        			</NavLink>
				        	</li>

				        	<li className="cl-effect-4">
				        		<NavLink to="/what-we-do" activeClassName="strategos-active">
				        			What We Do
				        		</NavLink>
				        	</li>

				        	<li className="cl-effect-4">
				        		<NavLink to="/blog" activeClassName="strategos-active">
				        			Blog
				        		</NavLink>
				        	</li>

				        	<li className="cl-effect-4">
				        		<NavLink to="/contact" activeClassName="strategos-active">
				        			Contact Us
			        			</NavLink>
				        	</li>

				        	<li className="button-style" >
				        		<NavLink to="/contact" className="lets_talk_button cl-effect-4_a">
				        			Let's Talk
				        		</NavLink>
				        	</li>
					      </ul>
					    </div>
					  </div>
					</nav>
			    </div>
			</Fade>
		);
	}
}

export default Header;
