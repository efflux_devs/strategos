import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';

class OurTeam extends Component {
	render() {
		return (
			<Slide left>
			    <div className="ourteam_section">
			    	<div className="background_color">
			    		<div className="container">
			    			<div className="row">
			    				<h1>
						    		Our Team
						    	</h1>

			    				<div className="col-md-6 strategos_col">
			    					<div className="strategos-team-adisa"></div>
			    					<h1>
			    						Wale Adisa
			    					</h1>
			    					<em>
			    						Managing Partner
			    					</em> <br /> <br />
			    					<p>
			    						Wale is the Managing Partner of the Strategos Company. <br /> <br />

										Before co-founding the Strategos Company, he was the Chief Executive Officer of CourierPlus Services Limited, ...
			    					</p>
			    					<Link to="/our-company">
			    						<i>Read More></i>
			    					</Link>
			    				</div>

			    				<div className="col-md-6">
			    					<div className="strategos-team-dipe"></div>
			    					<h1>
			    						Oludare Dipe
			    					</h1>
			    					<em>
			    						Cofounder and Partner
			    					</em> <br /> <br />
			    					<p>
			    						Oludare is a Cofounder and Partner at the Strategos Company. <br /> <br />

										Before co-founding the Strategos Company, he was the Chief Executive Officer and founder of QE Distributions...
			    					</p>
			    					<Link to="/our-company">
			    						<i>Read More></i>
			    					</Link>
			    				</div>
			    			</div>
			    			<div className="button">
		    					<Link to="/our-company">
		    						Let's talk
		    					</Link>
		    				</div>
			    		</div>
			    	</div>
			    </div>
			</Slide>
		);
	}
}

export default OurTeam;
