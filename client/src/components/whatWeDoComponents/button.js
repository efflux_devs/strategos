import React, { Component } from 'react';
import {
	Link
} from 'react-router-dom';


class WhatWeDoButton extends Component {
	render() {
		return (
		    <div className="button_section">
		    	<div className="background_color">
		    		<div className="container">
						<div className="row">
							<div className="col-md-12">
								<Link to="/contact" className="btn btn_primary">Talk to us</Link>
							</div>
						</div>
					</div>
		    	</div>
		    </div>
		);
	}
}

export default WhatWeDoButton;