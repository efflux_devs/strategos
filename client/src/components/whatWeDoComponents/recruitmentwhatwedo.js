import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';

class RecruitmentWhatWeDo extends Component {
	render() {
		return (
			<Slide left>
			    <div className="strategywhatwedo_section">
			    	<div className="background_color">
			    		<div className="container">
			    			<div className="row">
			    				<div className="col-md-12">
			    					<h1>
							    		Recruitment
							    	</h1>
			    					<p>
			    						<b>Executive Search: </b> 
			    						The impact a wrong hire at the executive level has on a business is catastrophic. It causes loss of 
			    						confidence in the leadership both internally and externally, as well as lowered productivity across 
			    						the business - as the time and resources required to onboard a replacement can distract attention from 
			    						running the business. We work with our clients to guarantee that they hire right at these levels by 
			    						ensuring that we identify candidates whose experience and personality would excel the most in the role 
			    						and fit the company culture. We ensure that we source for candidates that will understand and be comfortable 
			    						ith the internal realities of our client businesses.
			    					</p><br />
			    					<p>
			    						<b>Leadership Recruitment: </b>
			    						An organisation’s ability to execute its strategy is driven majorly by its middle management cadre 
			    						as they make decisions about resource allocation that are central and strategic, albeit at a lower 
			    						level and with much less visibility than decisions made by senior managers. Middle managers translate 
			    						the strategy into day-to-day operations and the stronger they are, the better the quality of decisions 
			    						they make for the business. We help our clients identify and recruit candidates for their middle management 
			    						who have the experience, subject matter expertise, and personality needed to succeed and drive performance 
			    						for them.
			    					</p><br />
			    					<p>
			    						<b>Technology Recruitment: </b>
			    						The technology talent recruitment space has become a seller's market with companies not only 
			    						competing with other local and foreign employers for the limited pool of experienced talent but 
			    						also increasingly having to deal with the growing disconnect between expected remuneration and actual 
			    						competence level. It also doesn't help that most employers do not know how to access technical expertise 
			    						in this space and end up paying too much for too little. We bring our working knowledge of the technology 
			    						space to bear in helping our clients firstly determine their actual talent requirements depending on what 
			    						they are looking to do and secondly helping them resource the talent they need to achieve their 
			    						desired outcome at the appropriate value.
			    					</p>
			    					<p>
			    						<b>Executive support Recruitment: </b>
			    						Executives are usually the most ineffective staff in companies not because they are not productive
			    						 with regards to work output, but rather because they spend an inordinate amount of time on urgent, 
			    						 unimportant and operational/ tactical issues that no one else seems to be able to handle. This 
			    						 leads to the classic C-level quandary where despite having a lot of subordinates, the executive 
			    						 still suffers from upwards delegation where he does everybody’s work but his own.   We work with 
			    						 our clients to escape this conundrum by helping find and onboard fit-for-purpose candidates for 
			    						 the chief of staff and executive assistant hires who work for the executive and focus singularly 
			    						 on optimizing their performance by ensuring that any distracting issue is taken care of in a 
			    						 manner that the executive would appreciate. 
			    					</p>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
		    </Slide>
		);
	}
}

export default RecruitmentWhatWeDo;
