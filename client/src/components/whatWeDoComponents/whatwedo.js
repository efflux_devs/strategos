import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';

class WhatWeDoWhatWeDo extends Component {
	render() {
		return (
			<Slide right>
			    <div className="whoweare_section">
			    	<div className="background_color">
			    		<div className="container">
			    			<div className="row">
			    				<h1>
						    		What We Do
						    	</h1>

			    				<div className="col-md-12">
			    					<p>
			    						We work with industry-defining businesses to help solve challenges their strategy 
			    						execution issues pragmatically. Through our strategy and execution, recruitment and 
			    						strategic communication offerings we help our clients build the capacity they need to 
			    						get them through the changes of today and deliver the results that define their tomorrow. 
			    						We help our clients turn their strategic aspirations into operational reality.							
			    					</p>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</Slide>
		);
	}
}

export default WhatWeDoWhatWeDo;
