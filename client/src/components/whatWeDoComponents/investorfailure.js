import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';
class InvestorFailure extends Component {
	render() {
		return (
		    <div className="investorfailure_banner">
		    	<div className="recruitmentfailure_banner_overlay"></div>
		    	<Slide up>
			    	<div className="strategyfailure_banner_caption">
			    		<div className="strategyfailure_content">
			    			<h1>
				    			It doesn't matter how good you, if nobody knows about you.
				    		</h1>
				    		<br />
			    			<p>
				    			Businesses need to distinguish themselves in their market and when that market is competitive, 
				    			it becomes even more of an issue. For brands looking to find an edge, having positive brand 
				    			perception is key to being remarkable. We live in an era where the perception of a brand is no 
				    			longer reliant on the quality of a product. Instead, a brand’s reputation is reliant on the perceived 
				    			value to its customers and expands much further than whether or not the product works. 
				    			We work with our clients to develop and execute a messaging strategy that helps position them in such 
				    			a manner that their brand perception improves.<br /> <br />

								<b>
									Ready to make a change in your business? 
									<Link to="/contact" className="strategos-bold-link"> TALK TO US.</Link>
								</b>
				    		</p>
			    		</div>
			    	</div>
			    </Slide>
		    	<div className="banner_img">
		    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
		    		<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
		    	</div>
		    </div>
		);
	}
}

export default InvestorFailure;