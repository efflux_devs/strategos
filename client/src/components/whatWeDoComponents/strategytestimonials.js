import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';

class StrategyTestimonials extends Component {
	render() {
		return (
		    <div className="strategytestimonials_section">
		    	<div className="background_color">
					<div className="container">
						<div className="row">
							<Slide left>
								<div className="col-md-6">
									<h1>
							    		Client Testimonials.
							    	</h1>
							    	<Link to="/contact" className="a">
							    		Talk to us
							    	</Link>
								</div>
							</Slide>
							<Slide left>
								<div className="col-md-6">
									<div className="carousel slide" data-ride="carousel" id="quote-carousel">
									    <div className="carousel-inner text-center">
									        <div className="item active">
									            <blockquote>
									                <div className="row">
									                    <div className="col-sm-8 col-sm-offset-2">
									                        <p className="testimonial_p">
									                        	We had tried to implement a performance management system before which 
									                        	failed to gain any traction, and we were quite apprehensive about working 
									                        	with another set of consultants. However, working with The Strategos Company 
									                        	was a totally different experience as they approached the engagement in a 
									                        	practical and outcome-focused manner, the performance system was in place 
									                        	and running in three months and the support we still get is amazing. 
									                        	We definitely will work with them again.
									                        </p>
									                        <small>Managing Director of Leading Healthcare Business</small>
									                    </div>
									                </div>
									            </blockquote>
									        </div>
									    </div>
									    <a data-slide="prev" href="#quote-carousel" className="left carousel-control"><i class="fa fa-chevron-left"></i></a>
									    <a data-slide="next" href="#quote-carousel" className="right carousel-control"><i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
							</Slide>
						</div>
					</div>
		    	</div>
		    </div>
		);
	}
}

export default StrategyTestimonials;