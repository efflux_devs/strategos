import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';

class InvestorWhatWeDo extends Component {
	render() {
		return (
			<Slide left>
			    <div className="investorwhatwedo_section">
			    	<div className="background_color">
			    		<div className="container">
			    			<div className="row">
			    				<div className="col-md-12">
			    					<h1>
							    		Stakeholder Management
							    	</h1>
			    					<p>
			    						<b>Investor Communications Strategy: </b> 
			    						The markets respond to businesses based on how they perceive them and perceptions are formed based on how 
			    						the story is told. Financial reports by themselves do not tell a wholesome story about where a business is 
			    						currently positioned on its journey and could be communicating the wrong message. We work with businesses to 
			    						craft compelling stories that position them positively in the minds of existing and potential investors by 
			    						utilizing our communications and financial analysis expertise to communicate clear linkages between the 
			    						businesses key messages and the expectations of the intended audience. We work with our clients to inform 
			    						their stakeholders about their financial performance; operational performance; ongoing strategic initiatives 
			    						and future prospects.
			    					</p><br />
			    					<p>
			    						<b>Investor Presentation: </b>
			    						People invest in what they know, therefore, the more they understand the service or product being offered, 
			    						how it is sold and the size of its addressable market, the more likely they are to invest in it. We help 
			    						businesses build investor presentations that they use to concisely illustrate both their growth potential 
			    						and rising equity valuation by building a compelling story that clearly outlines the strengths and weaknesses 
			    						of the business model; how the business intends to extract value from the opportunity and also mitigate against 
			    						the possible downsides.
			    					</p><br />
			    					<p>
			    						<b>Annual Report Writing: </b>
			    						When seeking to raise capital from either the equity or debt markets, the annual report is probably the 
			    						most important marketing collateral that a business can have as it not only shares financial hard numbers 
			    						but also allows an investor to gain insight into what the business strategy is and how well thought out the 
			    						vision of the future is.  We work with businesses to design, develop and produce annual reports that 
			    						convincingly communicates to investors a clear story about how the business seeks to grow the valuation 
			    						of the business and its ability to sustainable pay dividends in a visually appealing manner.
			    					</p>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</Slide>
		);
	}
}

export default InvestorWhatWeDo;
