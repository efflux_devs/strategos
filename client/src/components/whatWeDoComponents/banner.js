import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {Helmet} from "react-helmet";
import {
	Link
} from 'react-router-dom';

const TITLE = 'What We Do - Strategos'


class WhatWeDoBanner extends Component {
	render() {
		return (
		    <div className="whatwedo_banner">
		    	<div className="whatwedo_banner_overlay"></div>
		    	<Helmet>
	                <title> { TITLE } </title>
	            </Helmet>
	            <Slide left>
			    	<div className="ourcompany_banner_caption">
			    		<h1>
			    			What We Do
			    		</h1>
			    		<p>
			    			Our practice is built around the specific areas that we possess <br/>
			    			deep subject matter expertise as a result of our experience. <br/>
			    			Our practice is built around the specific areas that we possess <br/>
			    			deep subject matter expertise as a result of our experience. 
			    		</p>
			    	</div>
			    </Slide>

			    	<div className="banner_img">
			    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
			    		<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
			    	</div>
		    </div>
		);
	}
}

export default WhatWeDoBanner;