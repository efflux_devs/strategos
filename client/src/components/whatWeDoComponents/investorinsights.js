import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';

class InvestorInsights extends Component {
	render() {
		return (
		    <div className="investorinsights_section">
		    	<div className="background_color">
					<div className="container">
						<div className="row">
							<div className="col-md-12">
								<h1>
						    		Insights
						    	</h1>
							</div>
						</div>
					</div>
			    	<div className="container">
			    		<div className="row">
				    		<Slide left>
				    			<div className="col-md-3">
				    				<a href="/img/10-036.pdf" target="_blank">
				    					<div class="card">
											<div className="img"></div>
											<div class="card-body">
											    <h5 class="card-title">
											    	The B2B Elements of Value
											    </h5>
											    <p class="card-text">
											    	Some quick example text to build on the card title and make up the bulk of the card's content.<br />
											    	Some quick example text to build on the card title and make up the bulk of the card's content.
										    	</p>
										  	</div>
										</div>
				    				</a>
				    			</div>
				    		</Slide>
				    		<Slide left>
				    			<div className="col-md-3">
				    				<a href="/img/10-036.pdf" target="_blank">
				    					<div class="card">
											<div className="img"></div>
											<div class="card-body">
											    <h5 class="card-title">
											    	The B2B Elements of Value
											    </h5>
											    <p class="card-text">
											    	Some quick example text to build on the card title and make up the bulk of the card's content.<br />
											    	Some quick example text to build on the card title and make up the bulk of the card's content.
										    	</p>
										  	</div>
										</div>
				    				</a>
				    			</div>
				    		</Slide>
				    		<Slide left>
				    			<div className="col-md-3">
				    				<a href="/img/10-036.pdf" target="_blank">
				    					<div class="card">
											<div className="img"></div>
											<div class="card-body">
											    <h5 class="card-title">
											    	The B2B Elements of Value
											    </h5>
											    <p class="card-text">
											    	Some quick example text to build on the card title and make up the bulk of the card's content.<br />
											    	Some quick example text to build on the card title and make up the bulk of the card's content.
										    	</p>
										  	</div>
										</div>
				    				</a>
				    			</div>
				    		</Slide>
				    		<Slide left>
				    			<div className="col-md-3">
				    				<a href="/img/10-036.pdf" target="_blank">
				    					<div class="card">
											<div className="img"></div>
											<div class="card-body">
											    <h5 class="card-title">
											    	The B2B Elements of Value
											    </h5>
											    <p class="card-text">
											    	Some quick example text to build on the card title and make up the bulk of the card's content.<br />
											    	Some quick example text to build on the card title and make up the bulk of the card's content.
										    	</p>
										  	</div>
										</div>
				    				</a>
				    			</div>
				    		</Slide>
			    		</div>
			    	</div>
		    	</div>
		    </div>
		);
	}
}

export default InvestorInsights;