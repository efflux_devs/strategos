import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';

class StrategyWhatWeDo extends Component {
	render() {
		return (
			<Slide left>
			    <div className="strategywhatwedo_section">
			    	<div className="background_color">
			    		<div className="container">
			    			<div className="row">
			    				<div className="col-md-12">
			    					<h1>
							    		Strategy & Execution
							    	</h1>
			    					<p>
			    						<b>Performance Management: </b> 
			    						Market conditions usually change between strategic planning cycles negating some of the assumptions 
			    						that businesses make about the market they operate in and by extension their ability to achieve their 
			    						performance projections. <br />
										We enable businesses to achieve their performance targets by working with them as project managers, 
										impartial to internal company politics, to ensure end-to-end project oversight and delivery of agreed 
										strategic deliverables. <br />
										This ensures that businesses meet their performance targets by, even in the case that the data trend 
										projects underperformance,  enabling the business to carry out a strategic course correction.
			    					</p><br />
			    					<p>
			    						<b>Business Model Improvement: </b>
			    						Businesses with operating models that support the implementation of their value propositions tend 
			    						to outperform the competition. Businesses tend to start out with high operating model alignment but 
			    						as the business engages with the realities of the market, strategic assumptions change and if not reviewed,
			    						 the model quickly becomes misaligned. 
										We work with businesses to re-articulate their value propositions, targeted market segment, 
										product offering, and revenue model; and then help re-design their operating model to ensure optimal 
										performance across the business through competitive positioning and choice of value creation channels. <br />
										This enables businesses to outperform competition and achieve both short term profitability and 
										long term sustainable growth.
			    					</p><br />
			    					<p>
			    						<b>Budgeting & Financial Planning: </b>
			    						In today’s business environment the statistics-based approach to budgeting where the previous 
			    						year's numbers are used as the base to project the current year's financials is no longer as 
			    						relevant due to the accelerating pace of innovation-driven changes within markets. 
										We work with businesses to build zero-based budgets that are tied to their strategy’s value 
										drivers and which allow them project growth, unrelated to historical performance. 
										This approach also allows for both re-forecasting, as most budgets become obsolete in months, 
										and for actionable reporting to the leadership team. 
			    					</p>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
		    </Slide>
		);
	}
}

export default StrategyWhatWeDo;
