import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';

class InvestorTestimonials extends Component {
	render() {
		return (
		    <div className="investortestimonials_section">
		    	<div className="background_color">
					<div className="container">
						<div className="row">
							<Slide left>
								<div className="col-md-6">
									<h1>
							    		Client Testimonials.
							    	</h1>
							    	<Link to="/contact" className="a">
							    		Talk to us
							    	</Link>
								</div>
							</Slide>
							<Slide left>
								<div className="col-md-6">
									<div className="carousel slide" data-ride="carousel" id="quote-carousel">
									    <div className="carousel-inner text-center">
									        <div className="item active">
									            <blockquote>
									                <div className="row">
									                    <div className="col-sm-8 col-sm-offset-2">
									                        <p className="testimonial_p">
									                        	The Strategos Company developed a highly customised public relations 
									                        	strategy for us. They showed a deep understanding of our businesses 
									                        	objectives, they developed story ideas and media angles that helped build 
									                        	our profile with the right audience and they are always on the lookout 
									                        	for PR opportunities for us and how we can get the most out of them. 
								                        		The regular communication and updates were critical and we appreciate 
									                        	the responsiveness, personalised service and commitment to helping our 
									                        	company build our brand.
								                        	</p>
									                        <small> 
									                        	Managing Director, Training and Development Company
									                        </small>
									                    </div>
									                </div>
									            </blockquote>
									        </div>
									    </div>
									    <a data-slide="prev" href="#quote-carousel" className="left carousel-control"><i class="fa fa-chevron-left"></i></a>
									    <a data-slide="next" href="#quote-carousel" className="right carousel-control"><i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
							</Slide>
						</div>
					</div>
		    	</div>
		    </div>
		);
	}
}

export default InvestorTestimonials;