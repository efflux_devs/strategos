import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';
class StrategyFailure extends Component {
	render() {
		return (
		    <div className="strategyfailure_banner">
		    	<div className="whatwedo_banner_overlay"></div>
		    	<Slide up>
			    	<div className="strategyfailure_banner_caption">
			    		<div className="strategyfailure_content">
			    			<h1>
				    			Pay for Performance and not Attendance.
				    		</h1>
				    		<br />
			    			<p>
				    			Inability to focus on whats strategic and important, continuous inability to meet agreed performance 
				    			goals, incoherence in business operations and disconnect between organisational goals and employee 
				    			behaviour.<br /> <br />

								These are symptoms of strategic incoherence within the business, as a result of a misalignment 
				    			between what the business intends to achieve and the way it has been set up to achieve it. We work with 
				    			clients to articulate and execute practical strategic execution plans that help ensure that they achieve 
				    			their espoused goals. <br /> <br />

								<b>
									Ready to make a change in your business? 
									<Link to="/contact" className="strategos-bold-link"> TALK TO US.</Link>
								</b>
				    		</p>
			    		</div>
			    	</div>
			    </Slide>
		    	<div className="banner_img">
		    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
		    		<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
		    	</div>
		    </div>
		);
	}
}

export default StrategyFailure;