import React, { Component } from 'react';
import {
	Link
} from 'react-router-dom';


class StrategyBanner extends Component {
	render() {
		return (
		    <div className="strategy_banner">
		    	<div className="strategy_banner_overlay"></div>
		    	<div className="strategy_banner_caption">
		    		<h1>
		    			Strategy And Execution
		    		</h1>
		    		<p>
		    			Our practice is built around the specific areas that we possess <br/>
		    			deep subject matter expertise as a result of our experience. <br/>
		    		</p>
		    	</div>

		    	<div className="banner_img">
		    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
		    		<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
		    	</div>
		    </div>
		);
	}
}

export default StrategyBanner;