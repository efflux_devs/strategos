import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';

import {
	Link
} from 'react-router-dom';


class WhatWeDoServices extends Component {
	render() {
		return (
		    <div className="services_section">
		    	<div className="background_color">
		    		<div className="container">
						<div className="row">
							<Slide left>
								<div className="col-md-4">
									<div className="card">
									  <div className="strategos-strategy"></div>
									  <div className="card-body">
									    <h5 className="card-title">Strategy & Execution</h5>
									    <span className="half_border"></span>
									    <ul>
									    	<li>
									    		<i className="fa fa-caret-right"></i>
									    		Performance Management
								    		</li>
									    	<li>
									    		<i className="fa fa-caret-right"></i>
									    		Business Model Improvement
									    	</li>
									    	<li className="special_li">
									    		<i className="fa fa-caret-right"></i>
									    		Budgeting & Financial Planning
									    	</li>
									    </ul>
									    <Link to="/what-we-do/strategy-and-execution" className="btn btn_primary">More</Link>
									  </div>
									</div>
								</div>
							</Slide>

							<Slide left>
								<div className="col-md-4">
									<div className="card">
									  <div className="strategos-recruitment"></div>
									  <div className="card-body">
									    <h5 className="card-title">Recruitment</h5>
									    <span className="half_border"></span>
									    <ul>
									    	<li>
									    		<i className="fa fa-caret-right"></i>
									    		Executive Search
								    		</li>
									    	<li>
									    		<i className="fa fa-caret-right"></i>
									    		Leadership Recruitment
									    	</li>
									    	<li>
									    		<i className="fa fa-caret-right"></i>
									    		Technology Recruitment
									    	</li>
									    	<li>
									    		<i className="fa fa-caret-right"></i>
									    		Executive support Recruitment
									    	</li>
									    </ul>
									    <Link to="/what-we-do/recruitment" className="btn btn_primary">More</Link>
									  </div>
									</div>
								</div>
							</Slide>
							
							<Slide left>
								<div className="col-md-4">
									<div className="card">
									  <div className="strategos-investors"></div>
									  <div className="card-body">
									    <h5 className="card-title">
									    	Stakeholder Management
								    	</h5>
								    	<span className="half_border"></span>
									    <ul>
									    	<li>
									    		<i className="fa fa-caret-right"></i>
									    		Investor Communications Strategy
								    		</li>
									    	<li>
									    		<i className="fa fa-caret-right"></i>
									    		Investor Presentation
									    	</li>
									    	<li className="special_li">
									    		<i className="fa fa-caret-right"></i>
									    		Annual Report Writing
									    	</li>
									    </ul>
									    <Link to="/what-we-do/investor" className="btn btn_primary">More</Link>
									  </div>
									</div>
								</div>
							</Slide>
						</div>
					</div>
		    	</div>
		    </div>
		);
	}
}

export default WhatWeDoServices;