import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';
class StrategyFailure extends Component {
	render() {
		return (
		    <div className="recruitmentfailure_banner">
		    	<div className="recruitmentfailure_banner_overlay"></div>
		    	<Slide up>
			    	<div className="recruitmentfailure_banner_caption">
			    		<div className="recruitmentfailure_content	">
			    			<h1>
				    			First who, then what.
				    		</h1>
				    		<br />
			    			<p>
				    			People are not a company's best assets, the RIGHT PEOPLE are. 
				    			The ability of your business to execute its strategy flawlessly and outperform its competition is directly 
				    			tied to the quality of its people. Businesses based on their recruitment practices tend to have shallow 
				    			leadership benches. And the few capable senior hands tend to suffer from early burnout from the excessive 
				    			upward delegation and lack of support. Through our management and executive recruitment practice, 
				    			we have helped clients build out strong leadership and execution capabilities across their business.
				    			<br /> <br />

								<b>
									Ready to make a change in your business? 
									<Link to="/contact" className="strategos-bold-link"> TALK TO US.</Link>
								</b>
				    		</p>
			    		</div>
			    	</div>
			    </Slide>
		    	<div className="banner_img">
		    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
		    		<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
		    	</div>
		    </div>
		);
	}
}

export default StrategyFailure;