import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';


class InvestorBanner extends Component {
	render() {
		return (
		    <div className="investor_banner">
		    	<div className="whatwedo_banner_overlay"></div>
		    	<Slide left>
			    	<div className="recruitment_banner_caption">
			    		<h1>
			    			Stakeholder Management
			    		</h1>
			    		<p>
			    			Our practice is built around the specific areas that we possess <br/>
			    			deep subject matter expertise as a result of our experience. <br/>
			    		</p>
			    	</div>
			    </Slide>

		    	<div className="banner_img">
		    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
			    	<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
		    	</div>
		    </div>
		);
	}
}

export default InvestorBanner;