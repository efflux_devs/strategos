import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';

class ContactGoogleMap extends Component {
	render() {
		return (
			<Slide left>
			    <div className="googlemap_section">
			    	<div className="background_color">
			    		<div class="mapouter">
			    			<div class="gmap_canvas">
			    				<iframe width="600" height="600" id="gmap_canvas" 
			    				src="https://maps.google.com/maps?q=32b%20Ramat%20Crescent%2C%20Ogudu%20GRA%2C%20Ogudu%2C%20Lagos%2C%20Nigeria&t=&z=13&ie=UTF8&iwloc=&output=embed" 
			    				frameborder="0" scrolling="no" marginheight="0" marginwidth="0" title="STRATEGOS MAP"></iframe>
		    				</div>
	    				</div>
			    	</div>
			    </div>
			</Slide>
		);
	}
}

export default ContactGoogleMap;