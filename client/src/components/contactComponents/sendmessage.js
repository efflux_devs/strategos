import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Form,
	FormGroup,
	Input,
	Label
} from 'reactstrap';
import axios from 'axios';

const ContactSendMessageState = {
	name: '',
	email: '',
	phone: '',
	organisation: '',
	title: '',
	services: '',
	consultation: '',
	nameErr: '',
	emailErr: '',
	phoneErr: '',
	organisationErr: '',
	titleErr: '',
	servicesErr: '',
	consultationErr: '',

	result: []
}

class ContactSendMessage extends Component {
	constructor() {
		super()

		this.state = ContactSendMessageState;

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	handleChange = e => {
		this.setState({ [e.target.name]: e.target.value })
	}

	validate = () => {
		let nameErr = "";
		let emailErr = "";
		let phoneErr = "";
		let organisationErr = "";
		let titleErr = "";
		let servicesErr = "";
		let consultationErr = "";

		if (!this.state.name) {
			nameErr = "Name can not be blank";
		}

		if (!this.state.email) {
			emailErr = "Email address can not be blank";
		}

		if (this.state.email.includes("gmail")) {
			emailErr = "Please enter company email address not personal email address";
		}

		if (this.state.email.includes("yahoo")) {
			emailErr = "Please enter company email address not personal email address";
		}

		if (this.state.email.includes("hotmail")) {
			emailErr = "Please enter company email address not personal email address";
		}

		if (!this.state.phone) {
			phoneErr = "Phone Number can not be blank";
		}

		if (emailErr || nameErr) {
			this.setState({ emailErr, nameErr });
			return false;
		}

		return true;
	};

	async handleSubmit(e) {
		e.preventDefault();

		const isValid = this.validate();
		if (isValid) {
			console.log(this.state);

			// Clear Form
			this.setState(ContactSendMessageState);
		}

		// const { name, email, phone, organisation, title, services, consultation } = this.state

		// await axios.post('/customers/add', {
		// 	name,
		// 	email,
		// 	phone,
		// 	organisation,
		// 	title,
		// 	services,
		// 	consultation
		// })
		// .then(res => {
		// 	console.log(res.data);
		// 	this.setState({ result: res.data });
		// })
		// .catch(err => console.log(err));
	}

	render() {
		return (
			<Slide right>
			    <div className="sendmessage_section">
			    	<div className="background_color">
			    		<div className="container">
			    			<div className="row">
			    				<div className="col-md-6">
			    					<h1>
			    						How can we help?
			    					</h1>
			    					<span></span>
			    					<p>
			    						We work with serious businesses to help execute their strategy pragmatically.
			    					</p>
			    					<Form onSubmit={this.handleSubmit}>
			    						<FormGroup>
			    							<Label>Full Name</Label>
										    <Input 
										    type="text" 
										    name="name" 
										    onChange={this.handleChange} />
										    <p className="err_mssg">{this.state.nameErr}</p>
			    						</FormGroup>

			    						<FormGroup>
			    							<Label>Company Email</Label>
										    <Input 
										    type="email" 
										    name="email" 
										    onChange={this.handleChange} />
										    <p className="err_mssg">{this.state.emailErr}</p>
			    						</FormGroup>

			    						<FormGroup>
			    							<Label>Phone</Label>
										    <Input 
										    type="text" 
										    name="phone" 
										    onChange={this.handleChange} />
										    <p className="err_mssg">{this.state.phoneErr}</p>
			    						</FormGroup>

			    						<FormGroup>
			    							<Label>Organisation</Label>
										    <Input 
										    type="text" 
										    name="organisation" 
										    onChange={this.handleChange} />
										    <p className="err_mssg">{this.state.organisationErr}</p>
			    						</FormGroup>

			    						<FormGroup>
			    							<Label>Job Title</Label>
										    <Input 
										    type="text" 
										    name="title" 
										    onChange={this.handleChange} />
										    <p className="err_mssg">{this.state.titleErr}</p>
			    						</FormGroup>

			    						<FormGroup>
			    							<Label>What service are you interested in?</Label>
											<Input type="select" name="services" onChange={this.handleChange}>
									            <option>Investor Relations</option>
									            <option>Strategy & Execution</option>
									            <option>Recruitment</option>
								          	</Input>
								          	<p className="err_mssg">{this.state.servicesErr}</p>
			    						</FormGroup>

			    						<FormGroup>
			    							<Label>When can you be available for your consultation?</Label>
										    <Input 
										    type="date" 
										    name="consultation" 
										    onChange={this.handleChange} />
										    <p className="err_mssg">{this.state.consultationErr}</p>
			    						</FormGroup>

			    						<FormGroup>
			    							<Input type="checkbox" class="form-check-input" id="" />
										    <Label class="form-check-label policy">I have read the Privacy Policy and agree to its terms.</Label>
			    						</FormGroup>

			    						<p className="success_mssg">{this.state.result}</p>

									  	<button type="submit" class="btn btn_primary">Send</button>
									</Form>
			    				</div>
			    				<div className="col-md-6">
			    					<div className="parent">
			    						<h1>
				    						REACH US AT
				    					</h1>
				    					<p>
				    						08090335255, 07018794787 <br />
				    						<b>
				    							Email: engage@strategoscompany.com
				    						</b> 
				    						<b>
				    							32b Ramat Crescent, Ogudu GRA, Ogudu, <br />Lagos, Nigeria <br/>
				    						</b>
				    					</p>
				    					<div className="image">
				    						<div className="image_banner_overlay"></div>
				    					</div>
			    					</div>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</Slide>
		);
	}
}

export default ContactSendMessage;
