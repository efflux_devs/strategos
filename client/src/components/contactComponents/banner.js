import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {Helmet} from "react-helmet";
import {
	Link
} from 'react-router-dom';

const TITLE = 'Contact - Strategos'


class ContactBanner extends Component {
	render() {
		return (
			<div className="contact_banner">
			    	<div className="contact_banner_overlay"></div>
			    	<Helmet>
		                <title> { TITLE } </title>
		            </Helmet>
		            <Slide left>
				    	<div className="contact_banner_caption">
				    		<h1>
				    			Contact Us
				    		</h1>
				    		<p>
				    			Our practice is built around areas that we possess deep subject <br />
				    			matter expertise in, as a result of what we have done.
				    		</p>
				    	</div>
				    </Slide>
			    	<div className="banner_img">
			    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
			    		<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
			    	</div>
			</div>
		);
	}
}

export default ContactBanner;
