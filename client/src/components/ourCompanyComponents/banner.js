import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {Helmet} from "react-helmet";
import {
	Link
} from 'react-router-dom';

const TITLE = 'Our Company - Strategos'


class OurCompanyBanner extends Component {
	render() {
		return (
		    <div className="ourcompany_banner">
		    	<div className="ourcompany_banner_overlay"></div>
		    	<Helmet>
	                <title>{ TITLE }</title>
	            </Helmet>
		    	<div className="ourcompany_banner_caption">
		    		<h1>
		    			Our Company.
		    		</h1>
		    		<Slide left>
			    		<p>
			    			We are not consultants. We are managers with significant experience and <br/>
			    			understanding of what it takes to operate in volatile and uncertain markets. <br/>
			    			We are not consultants. We are managers with significant experience and <br/>
			    			understanding of what it takes to operate in volatile and uncertain markets.
			    		</p>
			    	</Slide>
		    	</div>
	    		<div className="banner_img">
		    		<Link to="/contact"  className="lets_talk_button cl-effect-4_a">Let's Talk</Link>
		    		<img src="/img/strategos-logo-without-button.png" className="stratelogo" alt="" />
		    	</div>
		    </div>
		);
	}
}

export default OurCompanyBanner;