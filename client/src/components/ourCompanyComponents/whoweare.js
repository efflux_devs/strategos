import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';

class OurCompanyWhoWeAre extends Component {
	render() {
		return (
			<Fade up>
			    <div className="whoweare_section">
			    	<div className="background_color">
			    		<div className="container">
			    			<div className="row">
			    				<h1>
						    		Who We Are
						    	</h1>

			    				<div className="col-md-12">
			    					<p>
			    						We are not consultants. We are managers with significant experience and understanding of 
			    						what it takes to operate in volatile and uncertain markets.  <br /> <br />

										Our C-level experience in different organizations and sectors has enabled us to build a 
										practical and outcome-based approach to managing businesses that we bring to bear upon 
										our clients’ pain-points. <br /> <br />

										We work with fast-growing businesses to ensure short-term profitability as well as long-term growth. 
										We achieve this by helping them take strategic choices that enable them to effectively leverage 
										their internal capabilities to out-compete other players in the markets they operate in; by focusing their 
										attention on ensuring that corporate performance targets are monitored, managed and met; and helping 
										them source and onboard fit-for-purpose talent that ensure they have the capacity required to execute 
										their strategy. <br /> <br />
										
										We also help businesses develop and execute bespoke investor relations strategies that clearly illustrate their growth potential and financial value to existing and potential investors.
			    					</p>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</Fade>
		);
	}
}

export default OurCompanyWhoWeAre;
