import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';


class OurCompanyOurTeam extends Component {
	render() {
		return (
		    <div className="ourcompanyourteam_section">
		    	<div className="background_color">
		    		<div className="container">
		    			<div className="row">
		    				<h1>
					    		Our Team
					    	</h1>
							
							<Slide left>
			    				<div className="col-md-6 strategos_col">
			    					<div className="strategos_team_adisa"></div>
			    					<h1>
			    						Wale Adisa
			    					</h1>
			    					<em>
			    						Managing Partner
			    					</em> <br /> <br />
			    					<p>
			    						Wale is the Managing Partner of the Strategos Company. <br /> <br />

										Before co-founding the Strategos Company, he was the Chief Executive Officer of 
										CourierPlus Services Limited, the largest indigenously owned courier company in 
										Nigeria and had prior to that held a number of executive level roles which include, 
										Director of Business Development at Konga online, Chief Operating Officer at CSL Stockbrokers 
										Limited and Head of Corporate Services at Wema Securities and Finance. <br /> <br />

										Wale has an MBA from Cranfield University School of Management and a Bachelor's 
										Degree in Agriculture from the University of Ilorin.
			    					</p>
			    				</div>
			    			</Slide>
							
							<Slide right>
			    				<div className="col-md-6">
			    					<div className="strategos_team_dipe"></div>
			    					<h1>
			    						Oludare Dipe
			    					</h1>
			    					<em>
			    						Cofounder and Partner
			    					</em> <br /> <br />
			    					<p>
			    						Oludare is a Cofounder and Partner at the Strategos Company. <br /> <br />

										Before co-founding the Strategos Company, he was the Chief Executive Officer 
										and founder of QE Distributions Limited a national trucking business and had prior 
										to that held other senior roles which include; Vice President of Fulfilment Logistics 
										in Konga Online Shopping Limited and Programs Coordinator at Hygeia Community Health Care. <br /> <br />

										Oludare has an MSc in Procurement, Logistics and Supply Chain Management from the 
										University of Salford and a Bachelor's Degree from the University of Ilorin.
			    					</p>
			    				</div>
			    			</Slide>
		    			</div>
		    			<div className="button">
	    					<Link to="/contact">
	    						Let's talk
	    					</Link>
	    				</div>
		    		</div>
		    	</div>
		    </div>
		);
	}
}

export default OurCompanyOurTeam;