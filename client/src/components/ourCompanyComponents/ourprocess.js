import React, { Component } from 'react';
import Slide from 'react-reveal/Slide';
import {
	Link
} from 'react-router-dom';


class OurCompanyOurProcess extends Component {
	render() {
		return (
		    <div className="ourprocess_section">
		    	<div className="background_color">
    				<h1>
			    		Our Process
			    	</h1>

			    	<div className="container">
			    		<div className="row">
			    			<Slide bottom>
				    			<div className="col-md-3">
				    				<img src="/img/strategos-engage-icon.png" className="stratelogo" alt=" Engage Icon "/>
				    				<h1>
				    					We Engage
				    				</h1>
				    				<span></span>

				    				<p>
				    					We facilitate purposeful conversations to help clearly 
				    					identify our clients business pain points and how it 
				    					prevents their optimal performance. 
				    				</p>
				    			</div>
			    			</Slide>
			    			<Slide bottom>
				    			<div className="col-md-3">
				    				<img src="/img/strategos-evolve-icon.png" className="stratelogo" alt=" Evaluate Icon "/>
				    				<h1>
				    					We Evolve
				    				</h1>
				    				<span></span>

				    				<p>
				    					We measure the effectiveness of solutions deployed, 
				    					ensuring that all goals set are met, and evaluate the end-results 
				    					depending on the solution provided.
				    				</p>
				    			</div>
				    		</Slide>
				    		<Slide bottom>
				    			<div className="col-md-3">
				    				<img src="/img/strategos-execute-icon.png" className="stratelogo" alt=" Evolve Icon "/>
				    				<h1>
				    					We Execute
				    				</h1>
				    				<span></span>

				    				<p>
				    					To ensure that the solution effectively addresses 
				    					identified needs, we work with the client to ascertain 
				    					the solution-user fit.
				    				</p>
				    			</div>
				    		</Slide>
				    		<Slide bottom>
				    			<div className="col-md-3">
				    				<img src="/img/strategos-evaluate-icon.png" className="stratelogo" alt=" Execute Icon "/>
				    				<h1>
				    					We Evaluate
				    				</h1>
				    				<span></span>

				    				<p>
				    					We measure the effectiveness of solutions deployed, 
				    					ensuring that all goals set are met, and evaluate the end-results 
				    					depending on the solution provided.
				    				</p>
				    			</div>
				    		</Slide>
			    		</div>
			    	</div>

    				<div className="button">
    					<Link to="/contact">
    						let's talk
    					</Link>
    				</div>
		    	</div>
		    </div>
		);
	}
}

export default OurCompanyOurProcess;
