import React, { Component } from 'react';
import {
	BrowserRouter as Router,
	Route
} from 'react-router-dom';
import { NavLink, Nav } from 'bootstrap';
import Header from './components/homeComponents/header';
import Banner from './components/homeComponents/banner';
import WhoWeAre from './components/homeComponents/whoweare';
import OurProcess from './components/homeComponents/ourprocess';
import OurTeam from './components/homeComponents/ourteam';
import OurClient from './components/homeComponents/ourclient';
import WhatWeDo from './components/homeComponents/whatwedo';
import NewsLetter from './components/homeComponents/newsletter';
import Footer from './components/homeComponents/footer';
import OurCompanyBanner from './components/ourCompanyComponents/banner';
import OurCompanyWhoWeAre from './components/ourCompanyComponents/whoweare';
import OurCompanyOurProcess from './components/ourCompanyComponents/ourprocess';
import OurCompanyOurTeam from './components/ourCompanyComponents/ourteam';
import WhatWeDoBanner from './components/whatWeDoComponents/banner';
import WhatWeDoWhatWeDo from './components/whatWeDoComponents/whatwedo';
import WhatWeDoServices from './components/whatWeDoComponents/services';
import WhatWeDoButton from './components/whatWeDoComponents/button';
import StrategyBanner from './components/whatWeDoComponents/strategybanner';
import StrategyWhatWeDo from './components/whatWeDoComponents/strategywhatwedo';
import StrategyInsights from './components/whatWeDoComponents/strategyinsights';
import StrategyTestimonials from './components/whatWeDoComponents/strategytestimonials';
import StrategyFailure from './components/whatWeDoComponents/strategyfailure';
import RecruitmentBanner from './components/whatWeDoComponents/recruitmentbanner';
import RecruitmentWhatWeDo from './components/whatWeDoComponents/recruitmentwhatwedo';
import RecruitmentInsights from './components/whatWeDoComponents/recruitmentinsights';
import RecruitmentTestimonials from './components/whatWeDoComponents/recruitmenttestimonials';
import RecruitmentFailure from './components/whatWeDoComponents/recruitmentfailure';
import InvestorBanner from './components/whatWeDoComponents/investorbanner';
import InvestorWhatWeDo from './components/whatWeDoComponents/investorwhatwedo';
import InvestorInsights from './components/whatWeDoComponents/investorinsights';
import InvestorTestimonials from './components/whatWeDoComponents/investortestimonials';
import InvestorFailure from './components/whatWeDoComponents/investorfailure';
import ContactBanner from './components/contactComponents/banner';
import ContactSendMessage from './components/contactComponents/sendmessage';
import ContactGoogleMap from './components/contactComponents/googlemap';
import ScrollToTop from './components/scrollToTopComponents/scrollToTop';
import './dist/homeComponentsStyle.css';
import './dist/ourCompanyComponentsStyle.css';
import './dist/whatWeDoComponentsStyle.css';
import './dist/contactComponentsStyle.css';
import './dist/font/font.css';

class App extends Component {
	render() {
		return (
			<Router>
				<ScrollToTop >
				    <div className="App">
				    	<Header />
				    		<Route exact path='/' component={Banner} />
				    		<Route exact path='/' component={WhoWeAre} />
				    		<Route exact path='/' component={OurProcess} />
				    		<Route exact path='/' component={OurTeam} />
				    		<Route exact path='/' component={OurClient} />
				    		<Route exact path='/' component={WhatWeDo} />
				    		<Route exact path='/our-company' component={OurCompanyBanner} />
				    		<Route exact path='/our-company' component={OurCompanyWhoWeAre} />
				    		<Route exact path='/our-company' component={OurCompanyOurProcess} />
				    		<Route exact path='/our-company' component={OurCompanyOurTeam} />
				    		<Route exact path='/what-we-do' component={WhatWeDoBanner} />
				    		<Route exact path='/what-we-do' component={WhatWeDoWhatWeDo} />
				    		<Route exact path='/what-we-do' component={WhatWeDoServices} />
				    		<Route exact path='/what-we-do' component={WhatWeDoButton} />
				    		<Route exact path='/what-we-do/strategy-and-execution' component={StrategyBanner} />
				    		<Route exact path='/what-we-do/strategy-and-execution' component={StrategyWhatWeDo} />
				    		<Route exact path='/what-we-do/strategy-and-execution' component={StrategyInsights} />
				    		<Route exact path='/what-we-do/strategy-and-execution' component={StrategyTestimonials} />
				    		<Route exact path='/what-we-do/strategy-and-execution' component={StrategyFailure} />
				    		<Route exact path='/what-we-do/recruitment' component={RecruitmentBanner} />
				    		<Route exact path='/what-we-do/recruitment' component={RecruitmentWhatWeDo} />
				    		<Route exact path='/what-we-do/recruitment' component={RecruitmentInsights} />
				    		<Route exact path='/what-we-do/recruitment' component={RecruitmentTestimonials} />
				    		<Route exact path='/what-we-do/recruitment' component={RecruitmentFailure} />
				    		<Route exact path='/what-we-do/investor' component={InvestorBanner} />
				    		<Route exact path='/what-we-do/investor' component={InvestorWhatWeDo} />
				    		<Route exact path='/what-we-do/investor' component={InvestorInsights} />
				    		<Route exact path='/what-we-do/investor' component={InvestorTestimonials} />
				    		<Route exact path='/what-we-do/investor' component={InvestorFailure} />
				    		<Route exact path='/contact' component={ContactBanner} />
				    		<Route exact path='/contact' component={ContactSendMessage} />
				    		<Route exact path='/contact' component={ContactGoogleMap} />
			    		<NewsLetter />
				    	<Footer />
				    </div>
			    </ScrollToTop>
		    </Router>
		);
	}
}

export default App;