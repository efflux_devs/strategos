// dependencies
const { src, dest, watch, series, parallel } = require('gulp');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const postcss = require('gulp-postcss');
const replace = require('gulp-replace');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

// file path variable
const files = {
	scssPath: 'src/app/scss/**/*.scss',
	jsPath: 'src/app/js/**/*.js'
}

// Sass Task
function scssTask() {
	return src(files.scssPath)
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(postcss([ autoprefixer(), cssnano() ]))
		.pipe(sourcemaps.write('.'))
		.pipe(dest('src/dist')
	);
}

// Js task
function jsTask() {
	return src(files.jsPath)
		.pipe(concat('all.js'))
		.pipe(uglify())
		.pipe(dest('src/dist')
	);
}

// Watch task
function watchTask() {
	watch(
		[files.scssPath, files.jsPath],
		parallel(scssTask. jsTask)
	);
}

// Default task
exports.default = series(
	parallel(scssTask, jsTask),
	// cacheBustTask,
	watchTask
);