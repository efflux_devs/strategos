const Sequelize = require('sequelize');
const db = require('../config/db.js');

const Customer = db.define('customers', {
	name: {
		type: Sequelize.STRING
	},
	email: {
		type: Sequelize.STRING
	},
	phone: {
		type: Sequelize.BIGINT
	},
	organisation: {
		type: Sequelize.STRING
	},
	title: {
		type: Sequelize.STRING
	},
	services: {
		type: Sequelize.STRING
	},
	consultation: {
		type: Sequelize.STRING
	}
});

module.exports = Customer;