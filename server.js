var express = require('express')
var exphbs = require('express-handlebars');
var bodyParser = require('body-parser');
var path = require('path');
var cors = require('cors');

// Database
const db = require('./config/db.js');

// Test Database
db.authenticate()
	.then(() => console.log('Database connected...'))
	.catch(err => console.log('Error: ' + err))


var app = express()


app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', (req, res) => res.send('INDEX'));

// Customers routes
app.use('/customers', require('./routes/customers'));

const PORT = process.env.PORT || 3001
app.listen(PORT, () => {
	console.log(`Server listening on port ${PORT}`)
})