var express = require('express');
var router = express.Router();
const db = require('../config/db');
const Customer = require('../models/Customer');


// Get Customers
// router.get('/', (req, res) =>
// 	Customer.findAll()
// 	.then(resp => {
// 		console.log(resp);
// 		res.sendStatus(200);
// 	})
// 	.catch(err => console.log(err)));


// Add Customer
router.post('/add', (req, res) => {
	let { name, email, phone, organisation, title, services, consultation } = req.body;

	// Insert into table
	Customer.create({
		name,
		email,
		phone,
		organisation,
		title,
		services,
		consultation
	})
		.then(customer => res.send('Inserted Successfully'))
		.catch(err => console.log(err));
});

module.exports = router;